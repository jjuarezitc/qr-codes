import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QrService {
  private skuList: string[];
  ITEMS_PER_CODE = 20;

  constructor() { }

  generateQRs(dataInput: string) {
    this.skuList = dataInput.split(',');

    let i: number;
    let j: number;
    let temparray: string[];
    const qrContentList: string[] = [];
    for (i = 0, j = this.skuList.length; i < j; i += this.ITEMS_PER_CODE) {
        temparray = this.skuList.slice(i, i + this.ITEMS_PER_CODE);
        qrContentList.push(temparray.toString());
    }
    return qrContentList;
  }
}
