import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { QrService } from 'src/app/services/qr.service';

@Component({
  selector: 'app-qr-generator',
  templateUrl: './qr-generator.component.html',
  styleUrls: ['./qr-generator.component.css']
})
export class QrGeneratorComponent implements OnInit {
  public dataInput: string = null;

  listsPerQR: string[] = [];

  constructor(private qrService: QrService) { }

  ngOnInit() { }

  generateQR(): void {
    this.listsPerQR = this.qrService.generateQRs(this.dataInput);
  }
}
